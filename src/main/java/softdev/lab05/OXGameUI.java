/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package softdev.lab05;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EliteCorps
 */
public class OXGameUI extends javax.swing.JFrame {

    /**
     * Creates new form OXGameUI
     */
    private Player player1, player2, currentPlayer;
    private gameBoard board;

    public OXGameUI() {
        initComponents();
        newGame();
    }

    private void newGame() {
        board = new gameBoard();
        this.player1 = new Player('X');
        this.player2 = new Player('O');
        currentPlayer = player1;
        loadGame();
        showWelcome();
        showBoard();
        showTurn();
        showStat();
    }

    private void showWelcome() {
        txtHeader.setText("Welcome to OX Game!!");
    }

    private void showBoard() {
        btnChar1.setText(board.boardChar[0][0] + "");
        btnChar2.setText(board.boardChar[0][1] + "");
        btnChar3.setText(board.boardChar[0][2] + "");
        btnChar4.setText(board.boardChar[1][0] + "");
        btnChar5.setText(board.boardChar[1][1] + "");
        btnChar6.setText(board.boardChar[1][2] + "");
        btnChar7.setText(board.boardChar[2][0] + "");
        btnChar8.setText(board.boardChar[2][1] + "");
        btnChar9.setText(board.boardChar[2][2] + "");
    }

    private void showTurn() {
        txtTurn1.setText("Current turn: " + currentPlayer.getSymbol());
    }

    private void switchTurn() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    private void process(int move) {
        if (board.checkValidMove(move)) {
            board.updateBoard(move, currentPlayer.getSymbol());
            showBoard();
            if (isEnd()) {
                showResult();
                showStat();
                saveGame();
            }
            switchTurn();
            showTurn();
        }
    }

    private void showResult() {
        if (board.checkWin()) {
            txtHeader.setText("Game Over!! " + currentPlayer.getSymbol() + "'s is the winner!!");
            if (currentPlayer == player1) {
                player1.win();
                player2.lose();
            } else {
                player1.lose();
                player2.win();
            }
        } else if (board.checkDraw()) {
            txtHeader.setText("Game Over!! It's a draw!!");
            player1.draw();
            player2.draw();
        }
    }

    private void showStat() {
        txtPlayerXWin1.setText("Win: " + player1.getWinCount());
        txtPlayerXDraw1.setText("Draw: " + player1.getDrawCount());
        txtPlayerXLose1.setText("Lose: " + player1.getLoseCount());
        txtPlayerOWin1.setText("Win: " + player2.getWinCount());
        txtPlayerODraw1.setText("Draw: " + player2.getDrawCount());
        txtPlayerOLose1.setText("Lose: " + player2.getLoseCount());
    }

    private boolean isEnd() {
        return board.checkWin() || board.checkDraw();
    }

    private void resetGame() {
        board = new gameBoard();
        currentPlayer = player1;
        showWelcome();
        showBoard();
        showTurn();
    }

    private void saveGame() {
        FileOutputStream fos = null;
        try {
            File file = new File("player.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(player1);
            oos.writeObject(player2);
            oos.close();
            fos.close();
        } catch (Exception ex) {
            System.out.println("Save Error");
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(OXGameUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void loadGame() {
        FileInputStream fis = null;
        try {
            File file = new File("player.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.player1 = (Player) ois.readObject();
            this.player2 = (Player) ois.readObject();
            ois.close();
            fis.close();
        } catch (Exception ex) {
            System.out.println("Load Error");
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(OXGameUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        btnNewGame1 = new javax.swing.JButton();
        txtPlayerO1 = new javax.swing.JLabel();
        txtTurn1 = new javax.swing.JLabel();
        txtPlayerX1 = new javax.swing.JLabel();
        txtPlayerXWin1 = new javax.swing.JLabel();
        txtPlayerXDraw1 = new javax.swing.JLabel();
        txtPlayerXLose1 = new javax.swing.JLabel();
        txtPlayerOWin1 = new javax.swing.JLabel();
        txtPlayerODraw1 = new javax.swing.JLabel();
        txtPlayerOLose1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        txtHeader = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnChar1 = new javax.swing.JButton();
        btnChar2 = new javax.swing.JButton();
        btnChar3 = new javax.swing.JButton();
        btnChar4 = new javax.swing.JButton();
        btnChar5 = new javax.swing.JButton();
        btnChar6 = new javax.swing.JButton();
        btnChar7 = new javax.swing.JButton();
        btnChar8 = new javax.swing.JButton();
        btnChar9 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("OX Game");

        jPanel3.setBackground(new java.awt.Color(255, 255, 204));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnNewGame1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnNewGame1.setText("New Game");
        btnNewGame1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewGame1ActionPerformed(evt);
            }
        });

        txtPlayerO1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        txtPlayerO1.setText("Player O");

        txtTurn1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtTurn1.setText("Current Turn: ");

        txtPlayerX1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        txtPlayerX1.setText("Player X");

        txtPlayerXWin1.setBackground(new java.awt.Color(204, 255, 204));
        txtPlayerXWin1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerXWin1.setText("Win:");
        txtPlayerXWin1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerXWin1.setOpaque(true);

        txtPlayerXDraw1.setBackground(new java.awt.Color(204, 204, 204));
        txtPlayerXDraw1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerXDraw1.setText("Draw:");
        txtPlayerXDraw1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerXDraw1.setOpaque(true);

        txtPlayerXLose1.setBackground(new java.awt.Color(255, 153, 153));
        txtPlayerXLose1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerXLose1.setText("Lose:");
        txtPlayerXLose1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerXLose1.setOpaque(true);

        txtPlayerOWin1.setBackground(new java.awt.Color(204, 255, 204));
        txtPlayerOWin1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerOWin1.setText("Win:");
        txtPlayerOWin1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerOWin1.setOpaque(true);

        txtPlayerODraw1.setBackground(new java.awt.Color(204, 204, 204));
        txtPlayerODraw1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerODraw1.setText("Draw:");
        txtPlayerODraw1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerODraw1.setOpaque(true);

        txtPlayerOLose1.setBackground(new java.awt.Color(255, 153, 153));
        txtPlayerOLose1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPlayerOLose1.setText("Lose:");
        txtPlayerOLose1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        txtPlayerOLose1.setOpaque(true);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnNewGame1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(txtPlayerXWin1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtPlayerXDraw1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtPlayerXLose1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtPlayerX1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtPlayerO1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtTurn1, javax.swing.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                    .addComponent(txtPlayerOWin1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtPlayerODraw1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(txtPlayerOLose1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 12, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtTurn1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtPlayerX1)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPlayerXWin1)
                    .addComponent(txtPlayerXDraw1)
                    .addComponent(txtPlayerXLose1))
                .addGap(33, 33, 33)
                .addComponent(txtPlayerO1)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPlayerOWin1)
                    .addComponent(txtPlayerODraw1)
                    .addComponent(txtPlayerOLose1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnNewGame1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(0, 51, 51));
        jPanel4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        txtHeader.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        txtHeader.setForeground(new java.awt.Color(255, 255, 255));
        txtHeader.setText("Welcome to OX Game!!");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(202, 202, 202)
                .addComponent(txtHeader)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(txtHeader)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(0, 204, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnChar1.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar1.setText("-");
        btnChar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar1ActionPerformed(evt);
            }
        });

        btnChar2.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar2.setText("-");
        btnChar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar2ActionPerformed(evt);
            }
        });

        btnChar3.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar3.setText("-");
        btnChar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar3ActionPerformed(evt);
            }
        });

        btnChar4.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar4.setText("-");
        btnChar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar4ActionPerformed(evt);
            }
        });

        btnChar5.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar5.setText("-");
        btnChar5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar5ActionPerformed(evt);
            }
        });

        btnChar6.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar6.setText("-");
        btnChar6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar6ActionPerformed(evt);
            }
        });

        btnChar7.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar7.setText("-");
        btnChar7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar7ActionPerformed(evt);
            }
        });

        btnChar8.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar8.setText("-");
        btnChar8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar8ActionPerformed(evt);
            }
        });

        btnChar9.setFont(new java.awt.Font("Segoe UI", 0, 24)); // NOI18N
        btnChar9.setText("-");
        btnChar9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChar9ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnChar1, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar2, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar3, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnChar4, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar5, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar6, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btnChar7, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar8, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnChar9, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChar1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar2, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChar4, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar5, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar6, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnChar7, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar8, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnChar9, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(31, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNewGame1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewGame1ActionPerformed
        resetGame();
    }//GEN-LAST:event_btnNewGame1ActionPerformed

    private void btnChar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar1ActionPerformed
        process(1);
    }//GEN-LAST:event_btnChar1ActionPerformed

    private void btnChar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar2ActionPerformed
        process(2);
    }//GEN-LAST:event_btnChar2ActionPerformed

    private void btnChar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar3ActionPerformed
        process(3);
    }//GEN-LAST:event_btnChar3ActionPerformed

    private void btnChar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar4ActionPerformed
        process(4);
    }//GEN-LAST:event_btnChar4ActionPerformed

    private void btnChar5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar5ActionPerformed
        process(5);
    }//GEN-LAST:event_btnChar5ActionPerformed

    private void btnChar6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar6ActionPerformed
        process(6);
    }//GEN-LAST:event_btnChar6ActionPerformed

    private void btnChar7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar7ActionPerformed
        process(7);
    }//GEN-LAST:event_btnChar7ActionPerformed

    private void btnChar8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar8ActionPerformed
        process(8);
    }//GEN-LAST:event_btnChar8ActionPerformed

    private void btnChar9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChar9ActionPerformed
        process(9);
    }//GEN-LAST:event_btnChar9ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(OXGameUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OXGameUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChar1;
    private javax.swing.JButton btnChar2;
    private javax.swing.JButton btnChar3;
    private javax.swing.JButton btnChar4;
    private javax.swing.JButton btnChar5;
    private javax.swing.JButton btnChar6;
    private javax.swing.JButton btnChar7;
    private javax.swing.JButton btnChar8;
    private javax.swing.JButton btnChar9;
    private javax.swing.JButton btnNewGame;
    private javax.swing.JButton btnNewGame1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel txtHeader;
    private javax.swing.JLabel txtPlayerO;
    private javax.swing.JLabel txtPlayerO1;
    private javax.swing.JLabel txtPlayerODraw;
    private javax.swing.JLabel txtPlayerODraw1;
    private javax.swing.JLabel txtPlayerOLose;
    private javax.swing.JLabel txtPlayerOLose1;
    private javax.swing.JLabel txtPlayerOWin;
    private javax.swing.JLabel txtPlayerOWin1;
    private javax.swing.JLabel txtPlayerX;
    private javax.swing.JLabel txtPlayerX1;
    private javax.swing.JLabel txtPlayerXDraw;
    private javax.swing.JLabel txtPlayerXDraw1;
    private javax.swing.JLabel txtPlayerXLose;
    private javax.swing.JLabel txtPlayerXLose1;
    private javax.swing.JLabel txtPlayerXWin;
    private javax.swing.JLabel txtPlayerXWin1;
    private javax.swing.JLabel txtTurn;
    private javax.swing.JLabel txtTurn1;
    // End of variables declaration//GEN-END:variables
}
